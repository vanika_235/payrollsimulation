/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab_6_software;

/**
 *
 * @author HP
 */
class Employee {
   
   
    private double wage;
    private String name;
    private double hours;
    
    Employee(String name, double wage, double hours){
        this.name=name;
        this.wage=wage;
        this.hours=hours;
    }
double calculatePay(){
    return wage*hours;
}
}

class Manager extends Employee{
    double bonus;
    
    Manager (String name, double wage, double hours, double bonus){
       super(name, wage, hours);
       this.bonus=bonus;
    }
    
    double calculatePay(){
        return super.calculatePay() +bonus;
        
    }
}

 